<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Requests\CreateUploadRequest;
use Input;
use File;

include(app_path() . '/libraries/spatie/pdfToImage/src/Pdf.php');

class PdfController extends Controller
{
    //
    public function store( CreateUploadRequest $request ){

      //$extension = Input::file('file')->getClientOriginalExtension();
      $filename = Input::file('file')->getClientOriginalName();
      $casualString = rand( 1111111, 999999999 ) . '_' . rand( 111, 99999999999 );
      $pathForSave =  public_path('Uploads') . '/' . $filename . '_' . $casualString;

      //creo i percorsi e alloco il file in una cartella univoca
      $creaCartellaCliente = File::makedirectory( $pathForSave  );
      $creaCartellaPNG = File::makedirectory( $pathForSave . '/png'  );
      Input::file('file')->move( $pathForSave, $filename );

      $PngPath = $pathForSave . '/png';

      //attivazione PDFtoImage per la creazione dei png di preview
       $pdfPage = new \Spatie\PdfToImage\Pdf( $pathForSave . '/' . $filename );
       $pagesQuantity = $pdfPage->getNumberOfPages();
        for( $i = 0; $i < $pagesQuantity; $i++ ){
          $pageNumber = $i + 1;
          $pdfPage->saveImage( $PngPath . '/' . '_pageNum_' . $pageNumber . '.png');
        }

      $pngToShowArray = array();

      $scannedPngPath = scandir($PngPath, 1);
      $scannedPngPath = array_reverse( $scannedPngPath );
      $scannedPngPath = array_slice( $scannedPngPath, 2 );
      foreach( $scannedPngPath as $paths => $path ){
          array_push( $pngToShowArray, $filename . '_' . $casualString . '/png/' .$path) ;
      }

      //print_r( $pngToShowArray );
      return view( 'pdf_editing.show_preview' )->with( 'pngPath', $pngToShowArray );

    }


}
