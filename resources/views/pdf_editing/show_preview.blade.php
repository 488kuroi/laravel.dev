@extends( 'pdf_test' )

@section( 'pdf_manipulate' )

<style media="screen">
  .imageWrapper{ display: none; }
</style>

<div class="container">
  <div class="col-md-10 col-md-offset-1 text-left">
      @if( count( $pngPath ) > 0 )
      <b style="font-size: 20px; ">La lista delle pagine</b>
        @foreach( $pngPath as $path )
        <div class="col-md-1 imageWrapper" style=""><img class="imageTocheck" style="max-height: 150px;" src="{{ asset('Uploads') . '/' . $path }}" alt="" /></div>
        @endforeach
        <div class="messageBox">

        </div>
      @else
      <b>Non ci sono immagini</b>
      @endif
    <div class="col-md-4">

    </div>
  </div>
</div>

<script type="text/javascript">

  $( document ).ready( function(){

    pageWith_Color_Quantity = 0;
    pageWith_No_Color_Quantity = 0;

    $imageTocheck = $( '.imageTocheck' );
    $imageTocheckCounter = $( '.imageTocheck' ).length;

    for( i = 0; i < $imageTocheckCounter; i++){
      var colorThief = new ColorThief();
      possiblePalette = colorThief.getColor( $imageTocheck.eq( i )[0], 3 );
      $r = possiblePalette[0];
      $g = possiblePalette[1];
      $b = possiblePalette[2];
      if( ( $r + $g + $b ) < 31 ){
        //console.log( 'è a in nero' );
        pageWith_No_Color_Quantity = pageWith_No_Color_Quantity + 1;
      }else{
        pageWith_Color_Quantity = pageWith_Color_Quantity + 1;
        $(this).parent().css( 'display', 'block' );
      }
    }

    $message = '<p> Sono presenti ' + pageWith_No_Color_Quantity +  ' pagine in bianco e nero</p>';
    $message += '<p> Sono presenti ' + pageWith_Color_Quantity +  ' pagine a colori</p>';

    $( '.messageBox' ).append( '' + $message + '' );


  } );

</script>



@stop
